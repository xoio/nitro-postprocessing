# nitro-postprocessing #

Contains a helper class to do postprocessing work on webgl scenes, primarily with Three.js.

This is written using ES6 style syntax which will require the use of [BabelJS](https://babeljs.io) or another ES6 transpiler in your project, to make changes.

### How do I get set up? ###

* pull down repo
* include it like any other npm module which should hopefully work if you
drag this to your `node_modules` folder. Eventually this will be up on npm
* Unlike the other modules, there is no Source.js file. The primarily base class is in `effects/PostProcessingEffect.js`
* Running `build.sh` will transpile all of the files in `effects` into ES5 compatible code.
