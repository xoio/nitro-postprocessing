"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var key in props) { var prop = props[key]; prop.configurable = true; if (prop.value) prop.writable = true; } Object.defineProperties(target, props); } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var PostProcessingEffect = (function () {
    function PostProcessingEffect(source, uniforms) {
        _classCallCheck(this, PostProcessingEffect);

        this.source = source !== undefined ? source : false;
        if (!source) {
            console.error("Post processing effect could not be created - no source");
            return false;
        } else {
            this.shader = new THREE.ShaderMaterial({
                vertexShader: ["varying vec2 vUv;", "void main() {", "\tvUv = vec2( uv.x, uv.y );", "\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );", "} "].join("\n"),
                fragmentShader: source,
                uniforms: uniforms
            });
        }

        this.uniforms = uniforms !== undefined ? uniforms : {};
        this._createSetters(uniforms);
    }

    _createClass(PostProcessingEffect, {
        isNitroPostProcessing: {

            //checks to see if this class is part of the nitro-postprocessing package

            value: function isNitroPostProcessing() {
                return true;
            }
        },
        getShader: {
            value: function getShader() {
                return this.shader;
            }
        },
        run: {
            value: function run(bundle, pass) {
                bundle.mesh.material = this.shader;
                bundle.renderer.render(bundle.scene, bundle.camera, pass.rt_3, false);
                bundle.renderer.render(bundle.scene, bundle.camera, pass.rt_3, false);
            }
        },
        _createSetters: {
            //builds setters for all of the uniform values

            value: function _createSetters(uniforms) {
                for (var i in uniforms) {
                    var funcName = i.charAt(0).toUpperCase() + i.slice(1);
                    var parent = this;
                    this.__proto__["set" + funcName] = function (value) {

                        //! TODO this should correctly reference the value in a pass but keep a eye out.
                        //! first part of "if" deals with if we're using three.js
                        if (parent.uniforms[i].hasOwnProperty("value")) {
                            parent.uniforms[i].value = value;
                            //if we're not using three.js
                        } else {
                            parent.uniforms[i] = value;
                        }

                        return this;
                    };
                }
            }
        }
    });

    return PostProcessingEffect;
})();

module.exports = PostProcessingEffect;
