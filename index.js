var BlurShader = require("./build/BlurShader.js");
var PostProcessingEffect = require("./build/PostProcessingEffect.js");

module.exports = {
    Effects:{
        Blur:BlurShader
    },
    PostProcessingEffect:PostProcessingEffect
}
