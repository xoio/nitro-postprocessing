
import PostProcessingEffect from "./PostProcessingEffect.js"
class BlurShader extends PostProcessingEffect {
    constructor(source){
        var uniforms = {
            texture:{
                type:"t",
                value:null
            },
            delta:{
                type:'v2',
                value:new THREE.Vector2()
            }
        }
        super(source,uniforms);
    }

    run(bundle,pass){

    }

}

export default BlurShader;
