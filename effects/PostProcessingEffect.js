class PostProcessingEffect {
    constructor(source,uniforms){
        this.source = source !== undefined ? source : false;
        if(!source){
            console.error("Post processing effect could not be created - no source");
            return false;
        }else{
            this.shader = new THREE.ShaderMaterial({
                vertexShader:[
                    'varying vec2 vUv;',

                    			'void main() {',
                    			'	vUv = vec2( uv.x, uv.y );',
                    			'	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
                    			'} '
                ].join("\n"),
                fragmentShader:source,
                uniforms:uniforms
            })
        }

        this.uniforms = uniforms !== undefined ? uniforms :{}
        this._createSetters(uniforms);
    }

    //checks to see if this class is part of the nitro-postprocessing package
    isNitroPostProcessing(){
        return true;
    }
    getShader(){
        return this.shader;
    }

    run(bundle,pass){
        bundle.mesh.material = this.shader;
        bundle.renderer.render( bundle.scene, bundle.camera, pass.rt_3, false );
        bundle.renderer.render( bundle.scene, bundle.camera, pass.rt_3, false );
    }
    //builds setters for all of the uniform values
    _createSetters(uniforms){
        for(var i in uniforms){
            var funcName = i.charAt(0).toUpperCase() + i.slice(1);
			var parent = this;
			this.__proto__["set" + funcName] = function(value){

				//! TODO this should correctly reference the value in a pass but keep a eye out.
				//! first part of "if" deals with if we're using three.js
				if(parent.uniforms[i].hasOwnProperty("value")){
					parent.uniforms[i].value = value;
					//if we're not using three.js
				}else{
					parent.uniforms[i] = value;
				}

                return this;
			}
        }
    }


}

export default PostProcessingEffect;
