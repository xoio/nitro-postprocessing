#!/bin/bash

#go into classes directory
SOURCE_DIR="cd effects";
$SOURCE_DIR;

for i in *.js; do
    command="babel $i --out-file ../build/$i"
    $command;
done
